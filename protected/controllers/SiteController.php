<?php

/**
 * Class SiteController
 *
 * Default website controller
 */
class SiteController extends Controller {

    public $obeder; 
    public $title = '';
    
    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => ['index', 'login'],
                'users' => array('?'),
            ),
            array('deny',
                'actions' => array('buy', 'unbuy', 'order', 'download', 'removeorder'),
                'users' => ['?'],
            ),
            array('deny',
                'actions' => array('delete'),
                'users' => array('*'),
            ),
        );
    }
    
    public function actionRegister(){
        $error = '';
        if(!empty($_POST)){
            
            
            $email = empty($_POST['username'])? '' : $_POST['username'];
            $pass = empty($_POST['password'])? '' : $_POST['password'];
            $nickname = empty($_POST['name'])? '' : $_POST['name'];
            $user = User::model()->findAllByAttributes(['mail'=>$email]);
            if(!empty($user)){
                $error = 1;
            }
            else{
                
                Controller::xmail($email, 
                    'Добро пожаловать в Obeder!', 
                    '<h1>Привет '.$nickname.'!</h1><p>Ты успешно зарегистрирован!</p>'
                    . '<p>Логин: '.$email.' Пароль: '.$pass.'</p>');
                
                $user = new User();
                $user->pswd = md5($pass);
                $user->mail = $email;
                $user->nickname = $nickname;
                $user->subscribe = 1;
                
                if($user->save()){
                    Yii::app()->runController('site/login');
                    return true;
                }
            }
            
        }
        
        $this->render('register', ['error'=>$error]);
    }

    /**
     * default action
     */
    public function init() {
        parent::init();
        $this->obeder = new Obeder();
        $this->obeder->checkMenu();
    }
    
    

    public function actionRecovery() {
        $error = 0;
        $sended = false;
        if(!empty($_POST['username'])){
            $mail = $_POST['username'];
            $user = User::model()->findByAttributes(['mail'=>$mail]);
            if(!empty($user)){
                $pswd = mt_rand(100000, 999999);
                $user->pswd = md5($pswd);                
                if($user->save()){
                    Controller::xmail($mail, 'Восстановление пароля', '<h1>Ваше новый пароль: '.$pswd.'</h1>');
                    $sended = true;
                }
            }else{
                $error = 1;
            }
        }
        
        
        $this->render('recovery', ['sended'=>$sended, 'error'=>$error]);
    }

    public function actionLogin() {
        $error = null;
        if ($_POST) {
            $identity = new UserID($_POST['username'], $_POST['password']);
            if ($identity->authenticate()) {
                Yii::app()->user->login($identity, 3600 * 24 * 31);
                $this->redirect(['site/index']);
            } else {
                $error = $identity->errorCode;
            }
        }

        $this->render('login', ['error' => $error]);
    }

    public function actionLogout() {
        if (!Yii::app()->user->isGuest)
            Yii::app()->user->logout();
        $this->redirect(['site/index']);
    }

    public function actionIndex($dayofweek = false) {
        if ($dayofweek == false) {
            $dayofweek = date('w') + 1;
            while (true) {
                $menu = $this->obeder->generateMenuToday($dayofweek);
                if ($menu != false || $dayofweek > 10) break;
                $dayofweek++;
            }

        } else {
            $menu = $this->obeder->generateMenuToday($dayofweek);
        }

        
        $result = false;
        //Если меню есть, тогда дергаем категории
        if ($menu != false) {
            $category = Category::model()->findAll();
            $result = [];
            foreach ($category as $cur) {
                $saveName = false;
                foreach ($menu as $m) {
                    if ($m['category'] == $cur->id) {
                        $result[$cur->id]['products'][] = $m;
                        $saveName = true;
                    }
                }
                if ($saveName) {
                    $result[$cur->id]['name'] = $cur->name;
                }
            }
        }

        $date = strtotime(Obeder::dowToDate($dayofweek));

        $this->render('index', [
            'menu' => $result,
            'dayofweek' => $dayofweek,
            'date' => $date,
        ]);
    }

    public function actionError($error = '') {
        $this->render('error', ['error' => !empty($error) ? $error : Yii::app()->errorHandler->error]);
    }

    public function actionUnbuy($id, $date, $dayofweek) {
        return $this->actionBuy($id, $date, $dayofweek, false);
    }

    public function actionBuy($id, $date, $dayofweek, $plusOne = true) {

        $order = new Order();
        $user_id = Yii::app()->user->id;
        $date = date('Y-m-d H:i:s', $date);
        
        $result = $order->find('product_id=:product_id AND user_id=:user_id AND date = CAST(:date AS DATE)', [
            ':date' => $date,
            ':user_id' => $user_id,
            ':product_id' => $id,
        ]);

        //var_dump($result);die;

        if (!empty($result)) {
            
            if ($plusOne == false) {
                if ($result->count < 2) {
                    $result->delete();
                } else {
                    $result->count--;
                }
            } else {
                $result->count++;
            }
            if (!$result->save()) {
                print_r($result->errors);
            }
        } else {
            $order->user_id = $user_id;
            $order->date = $date;
            $order->count = 1;
            $order->product_id = $id;

            if (!$order->save()) {
                
                print_r($order->errors);
            }
        }
        $this->redirect(['site/index', 'dayofweek'=>$dayofweek]);
    }

    public function actionStat() {
        return new Stat();
    }
    
    public function actionRemoveOrder($date, $id){
        if(!$this->isAdmin()) 
            return false;
        $order = Order::model()->findByPk($id);
        if(!empty($order)){
            if($order->delete()){
                $this->redirect(
                    [
                        'site/order', 
                        'date'=>date('d.m.Y', $date),
                    ]);
            }
        }
    }
    
    public function actionDownload(){
        $time = intval($_POST['download']);
        $date = date('Y-m-d H:i:s', $time);
        $order = Obeder::getAgregateOrder($date);
        Obeder::generateXL($order, $date);
    }

    public function actionOrder($date = null) {
        

        Yii::app()->clientScript->registerCssFile(Yii::app()->getBaseUrl() . '/css/datepicker.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->getBaseUrl() . '/css/jquery-ui.min.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->getBaseUrl() . '/css/jquery-ui.structure.min.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->getBaseUrl() . '/css/jquery-ui.theme.min.css');
        Yii::app()->clientScript->registerCss('order', '.admin-remove{display: none}', 'print');

        if ($date == null) {
            $date = Obeder::getDateByNextOrder();
            
        } else {
            $date = explode('.', $date);
            $date = date('Y-m-d H:i:s', strtotime($date[2] . '-' . $date[1] . '-' . $date[0]));
        }

        $users = Yii::app()->db->createCommand()->selectDistinct('m.user_id, u.nickname as name')
                ->from('order m')
                ->leftJoin('users u', 'u.id = m.user_id')
                ->where('date = CAST(:date AS DATE)', [
                    ':date' => $date
                ])->queryAll();

        $date = strtotime($date);

        if (!empty($users)) {

            $orders = [
                'sum' => 0,
                'users' => [],
            ];

            foreach ($users as $u) {
                $order = Obeder::getOrderByUser($u['user_id'], $date);
                if (!empty($order)) {
                    $orders['users'][$u['user_id']]['name'] = !empty($u['name']) ? $u['name'] : 'unknown';
                    $sum = 0;
                    foreach ($order as $o) {
                        $sum += $o['total'];
                    }
                    $orders['sum'] += $sum;
                    $orders['users'][$u['user_id']]['sum'] = $sum;
                    $orders['users'][$u['user_id']]['products'] = $order;
                }
            }
            if (!empty($orders['users'])) {
                if ($orders['sum'] <= 1000)
                    $orders['delivery'] = 200;
                elseif ($orders['sum'] > 1000 && $orders['sum'] <= 1500)
                    $orders['delivery'] = 100;
                elseif ($orders['sum'] > 1500)
                    $orders['delivery'] = 0;

                $orders['total'] = $orders['sum'] + $orders['delivery'];

                $deliverPersone = round($orders['delivery'] / sizeof($orders['users']));

                foreach ($orders['users'] as &$user) {
                    $user['total'] = $deliverPersone + $user['sum'];
                }
            } else {
                $orders = null;
            }
        } else {
            $orders = null;
        }

        $this->render('order', [
            'orders' => $orders,
            'date' => $date,
        ]);
    }

}
