<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Obeder
 *
 * @author basyrov
 */
class Obeder {

    public $objPHPExcel;
    public $xlsxFile = 's.xls';
    public $production = false;
    public $currentWeekUrl = 'http://hleb-sol.biz/templates/1.xls';
    public $nextWeekUrl = 'http://hleb-sol.biz/templates/2.xls';

    public function __construct() {
        if($_SERVER['DOCUMENT_ROOT'] == 'D:/web/domains/localhost/')
            $this->production = false;
        else
            $this->production = true;
        
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        Yii::import("ext.Classes.PHPExcel", true);
        $this->objPHPExcel = new PHPExcel();
        spl_autoload_register(array('YiiBase', 'autoload'));
        return true;
    }
    public function getXL($url = ''){
        if($this->production){
            return self::getRemote($url);
        }
        return self::getRemoteOverProxy($url);
    }
    
    
    static public function getRemoteOverProxy($url = ''){
        $loginpassw = 'basyrov:123qweasdV';
        $proxy_ip = '10.1.0.3';
        $proxy_port = '8080';        

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, $proxy_port);
        curl_setopt($ch, CURLOPT_PROXYTYPE, 'HTTP');
        curl_setopt($ch, CURLOPT_PROXY, $proxy_ip);
        curl_setopt($ch, CURLOPT_PROXYUSERPWD, $loginpassw);
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }
    
    static public function getRemote($url = ''){
        return file_get_contents($url);
    }

    static public function getPersonalMenu($dayofweek) {

        $date = self::dowToDate($dayofweek);
        
        
        $user_id = Yii::app()->user->getId();
        $result = Yii::app()->db->createCommand()
            ->select('p.*, m.id AS menu_id,o.count')
            ->from('menu m')
            ->leftJoin('product p', 'p.id = m.product_id')
            ->leftJoin('(SELECT * FROM `order` WHERE user_id = :user_id) o', 'm.product_id = o.product_id '
                . 'AND'
                . ' CAST(m.date AS DATE) = CAST(o.date AS DATE)', [':user_id' => $user_id])
            ->where('DATE(m.date) = CAST(:date AS DATE)', [':date' => $date])        
            ->queryAll();
        
        return $result;
    }

    static public function getMenu($dayofweek) {

        $date = self::dowToDate($dayofweek);

        if (!Yii::app()->user->isGuest)
            return self::getPersonalMenu($dayofweek);

        

        $menu = Yii::app()->db->createCommand()
                ->select('p.*, m.id as menu_id')
                ->from('menu m')
                ->leftJoin('product p', 'p.id = m.product_id')
                ->where('m.date = CAST(:date AS DATE)', [
                    ':date' => $date,
                ])->queryAll();

        return $menu;
    }

    static public function existProduct($hash) {
        $product = Product::model()->findByAttributes(['hash' => $hash]);
        if (!empty($product)) {
            return $product->id;
        }
        return false;
    }

    static public function dowToDate($dayofweek) {        
        $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

        if ($dayofweek > 5) {
            $dayofweek -= 5;
            $time = strtotime('next ' . $days[$dayofweek]);
        } else {
            $time = strtotime($days[$dayofweek] . ' this week');
        }

        return date('Y-m-d H:i:s', $time);
    }

    public function generateMenuToday($dayofweek) {

        $menu = Obeder::getMenu($dayofweek);
        if(!empty($menu)) return $menu;

        if ($dayofweek > 5) {
            $this->xlsxFile = 's2.xls';
            $dayofweek -= 5;            
        }
        $date = self::dowToDate($dayofweek);
        
        
        $xmls = Yii::app()->getRuntimePath() . DIRECTORY_SEPARATOR . $this->xlsxFile;
        
        if(file_exists($xmls)){
            unlink($xmls);
        }
        $date_sql = $date;
        $nextweek = false;
        if($this->xlsxFile == 's2.xls'){
            $nextweek = true;
            $date_sql = date('Y-m-d H:i:s', strtotime('+1 week'));            
        }
        
        $date_from = self::dateToDateDayOfWeek($date_sql, 1);
        $date_to = self::dateToDateDayOfWeek($date_sql, 7);
        
        $storage = Storage::model()->getMenu($date_from, $date_to);
        if(empty($storage)){
            return false;
        }
        file_put_contents($xmls, $storage);
        

        $inputFileType = PHPExcel_IOFactory::identify($xmls);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($xmls);
        $total_sheets = $objPHPExcel->getSheetCount();
        if ($total_sheets < 3) {
            return false;
        }
        $allSheetName = $objPHPExcel->getSheetNames();
        
        if($nextweek){
            $dateYMD = date('d.m.y', strtotime($date . ' +1 week'));
            $datePicked = date('Y-m-d H:i:s', strtotime($date . ' +1 week'));
        }
        else{
            $dateYMD = date('d.m.y', strtotime($date));
            $datePicked = date('Y-m-d H:i:s', strtotime($date));
        }
        
        $tabIndex = -1;  
        
        foreach($allSheetName as $id => $tabs){
            if(strpos($tabs, $dateYMD) !== false){
                $tabIndex = $id;
                break;
            }
        }
        
        if($tabIndex < 0)
            return false;
        
        $objWorksheet = $objPHPExcel->setActiveSheetIndex($tabIndex);
        $highestRow = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        if ($highestRow < 5) {
            return false;
        }
        $category_id = 0;
        for ($row = 0; $row < $highestRow; $row++) {

            //Считываем поля в строке
            $id = $objWorksheet->getCellByColumnAndRow(0, $row)->getValue();
            $name = $objWorksheet->getCellByColumnAndRow(1, $row)->getValue();
            $netto = $objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
            $price = $objWorksheet->getCellByColumnAndRow(3, $row)->getValue();

            //Фильтруем не продукты
            if (empty($price) || !is_numeric($price)) {
                //Если не продукт то вероятно это категория
                $category = Category::model()->findByAttributes(['name' => $id]);
                if (!empty($category)) {
                    $category_id = $category->id;
                } else {
                    $category = new Category();
                    $category->name = $id;
                    if ($category->save()) {
                        $category_id = $category->id;
                    }
                }
                continue;
            }
            if (empty($netto) || ($netto == '-'))
                $netto = 1;


            $hash = md5($name . $netto . $price);
            $product_id = self::existProduct($hash);
            if (false == $product_id) {
                $product = new Product();
                $product->hash = $hash;
                $product->name = $name;
                $product->weight = $netto;
                $product->price = $price;
                $product->category = $category_id;

                if (!$product->save()) {
                    print_r($product->errors);
                }
                $product_id = $product->id;
            }
            
            $menu = Menu::model()->find('date = CAST(:date AS DATE) AND product_id=:product_id', [
                ':date' => $datePicked,
                ':product_id' => $product_id,
            ]);
            

            if (empty($menu)) {
                $menu = new Menu();
                $menu->date = $datePicked;
                $menu->product_id = $product_id;
                $menu->save();
            }
        }
        return Obeder::getMenu($dayofweek);
    }

    static public function getOrderByUser($user_id, $date) {

        $order = Yii::app()->db->createCommand()
                ->select('p.*, o.count, (o.count*p.price) AS total, o.id as order_id')
                ->from('order o')
                ->leftJoin('product p', 'p.id = o.product_id')
                ->where('DATE(o.date) = CAST(:date AS DATE) AND
            o.user_id = :user_id', [
                    ':date' => date('Y-m-d H:i:s', $date),
                    ':user_id' => $user_id,
                ])->queryAll();
        return $order;
    }
    
    static public function getAgregateOrder($date){
        
        $order = Yii::app()->db->createCommand()
            ->select('p.hash, SUM(`o`.`count`) AS `count`')
            ->from('order o')
            ->leftJoin('product p', 'p.id = o.product_id')
            ->where('`date`=CAST(:date AS DATE)', [':date'=>$date])
            ->group('o.product_id')
            ->queryAll();
        
        if(!empty($order))
            return $order;
        return false;        
    }
    
    static public function generateXL($order = [], $date){
        
        $date_to    = self::dateToDateDayOfWeek($date, 7);
        $date_from    = self::dateToDateDayOfWeek($date, 1);
        $storage = Storage::model()->getMenu($date_from, $date_to);
        if(false == $storage) return false;
        
        
        $xmls = Yii::app()->getRuntimePath() . DIRECTORY_SEPARATOR . 't.xls';
        
        $xmls_edited = Yii::app()->getRuntimePath() . DIRECTORY_SEPARATOR . 't_edited.xls';
        
        if(file_exists($xmls))unlink ($xmls);
        if(file_exists($xmls_edited))unlink ($xmls_edited);
        
        
        
        file_put_contents($xmls, $storage);
        
        
        
        
        $inputFileType = PHPExcel_IOFactory::identify($xmls);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($xmls);
        $total_sheets = $objPHPExcel->getSheetCount();
        $allSheetName = $objPHPExcel->getSheetNames();
        
        $dateYMD = date('d.m.y', strtotime($date));
        
        $tabIndex = -1;  
        
        foreach($allSheetName as $id => $tabs){
            if(strpos($tabs, $dateYMD) !== false){
                $tabIndex = $id;
                break;
            }
        }
        
        
        if($tabIndex < 0){
            return false;
        }
        
        
        
        $objWorksheet = $objPHPExcel->setActiveSheetIndex($tabIndex);
        $highestRow = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

        $book = PHPExcel_IOFactory::load($xmls); //открываем файл
        $bookObj = $book->setActiveSheetIndex($tabIndex);
        
        
        foreach ($order as $cur) {
            
            for ($row = 0; $row < $highestRow; $row++) {
                
                $name = $objWorksheet->getCellByColumnAndRow(1, $row)->getValue();
                $netto = $objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
                $price = $objWorksheet->getCellByColumnAndRow(3, $row)->getValue();
                
                if (empty($netto) || ($netto == '-'))
                    $netto = 1;
                
                $hash = md5($name . $netto. $price);
                
                if ($cur['hash'] == $hash) {                    
                    $bookObj->setCellValue('E' . $row, $cur['count']);                    
                }
            }
        }

        $objWriter = PHPExcel_IOFactory::createWriter($book, 'Excel5');
        
        $objWriter->save($xmls_edited);
        
        if(file_exists($xmls_edited)){
            $order_name = 'order_'.date('Y-m-d', strtotime($date)).'.xls';
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=".$order_name);  //File name extension was wrong
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);            
            echo readfile($xmls_edited);            
            unlink($xmls_edited);
            unlink($xmls);
        }
        
        return true;
    }
    
    static public function dateToDateDayOfWeek($date, $dow){        
        $dayofweek = date('w', strtotime($date));
        return date('Y-m-d H:i:s', strtotime(($dow - $dayofweek).' day', strtotime($date)));        
    }
    
    public function checkMenu(){
        $date = date('Y-m-d H:i:s');
        
        $date_to    = self::dateToDateDayOfWeek($date, 7);
        $date_from    = self::dateToDateDayOfWeek($date, 1);
        
        
        $menu = Storage::model()->getMenu($date_from, $date_to);
        //Если меню нет в хранилище, скачиваем и сохраняем
        if(empty($menu)){
            $menuCurrentWeek = $this->getXL($this->currentWeekUrl);
            
            if(!empty($menuCurrentWeek)){
                $storage = new Storage();
                $storage->date = $date;
                $storage->menu = $menuCurrentWeek;
                $storage->save();
            }
        }
        
        if(date('w') >= 4){
            $date = date('Y-m-d H:i:s', strtotime('+7 day'));            
            $date_to    = self::dateToDateDayOfWeek($date, 7);
            $date_from    = self::dateToDateDayOfWeek($date, 1);
            
            $menu = Storage::model()->getMenu($date_from, $date_to);
            //Если меню нет в хранилище, скачиваем и сохраняем
            if(empty($menu)){
                $menuNextWeek = $this->getXL($this->nextWeekUrl);

                if(!empty($menuNextWeek)){
                    $storage = new Storage();
                    $storage->date = $date;
                    $storage->menu = $menuNextWeek;
                    $storage->save();
                }
            }
        }        
    }
    
    static public function getDateByNextOrder(){
        
        $result = Yii::app()->db->createCommand()
            ->select('date')
            ->from('order')
            ->where('`date` > NOW()')
            ->group('date')
            ->order('date ASC')
            ->limit(1)
            ->queryRow();
        
        if(!empty($result)){
            $date = $result['date'];
        }
        else{
            
            
            $date = Yii::app()->db->createCommand()
                ->select('IF(
                            (
                                SELECT WEEKDAY(NOW())) < 4 OR (SELECT WEEKDAY(NOW()) = 6),	
                                NOW() + INTERVAL 1 DAY,
		
                            (
                                SELECT IF(
                                (SELECT WEEKDAY(NOW())) = 5, 
                                NOW() + INTERVAL 2 DAY, NOW() + INTERVAL 3 DAY)		
                            )
                        ) AS `date`')
                ->queryScalar();            
            
        }
        
        return $date;
    }
    
    static public function currentDate($date){
        
        
        $dow = [
            'воскресенье','понедельник','вторник','среду', 'четверг', 'пятницу', 'субботу',
        ];
        return $dow[date('w', $date)];
    }

}
