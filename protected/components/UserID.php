<?php

class UserID extends CUserIdentity {

    private $_id;
    public $nickname;

    public function authenticate() {
        $record = User::model()->findByAttributes(array('mail' => $this->username));
        if ($record === null)
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        else if (md5($this->password) != $record->pswd)
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else {
            $this->_id = $record->id;
            $this->setState('nickname', $record->nickname);

            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }

    public function getId() {
        return $this->_id;
    }

}
