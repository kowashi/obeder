<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Controller extends CController {
    public function isAdmin() {
        $user = Yii::app()->user;
        $admins = [9, 14];
        if (in_array($user->id, $admins))
            return true;
        return false;
    }
    
    static public function xmail($to, $title,$message){
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;

        $mail->SetFrom('no-reply@obeder.acquiropay.com', 'Obeder');
        $mail->Subject = $title;            
        $mail->MsgHTML($message);
        $mail->AddAddress($to);
        return $mail->Send();
    }
    
}
