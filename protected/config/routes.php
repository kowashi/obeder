<?php

return array(
    'defaultController' => 'site/index',
    'gii' => 'gii',
    'gii/<controller:\w+>' => 'gii/<controller>',
    'gii/<controller:\w+>/<action:\w+>' => 'gii/<controller>/<action>',
    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
);
