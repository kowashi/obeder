<script src="<?php echo Yii::app()->getBaseUrl(); ?>/js/jquery-ui.min.js"></script>
<script>
    $(document).ready(function () {
        $.datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '&#x3c;Пред',
            nextText: 'След&#x3e;',
            currentText: 'Сегодня',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
                'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            dateFormat: 'dd.mm.yy',
            firstDay: 1,
            isRTL: false,
        };
        $.datepicker.setDefaults($.datepicker.regional['ru']);
        $('#date').datepicker({onSelect: function (selectedDate) {

                window.location.href = '<?php echo Yii::app()->createUrl('site/order') ?>&date=' + $(this).val();
            }
        });
    });
</script>


<h3>Список заказов на <u><input id="date" value="<?php echo date('d.m.Y', $date); ?>" name="date" /></u></h3>
<?php if (empty($orders)): ?>
    <h3 class="bg-warning">Нет заказов на <?php echo date('d.m.Y', $date); ?></h3>
<?php else: ?>
    <?php foreach ($orders['users'] as $cur): ?>
        <h4><?php echo $cur['name']; ?></h4>

        <table class="table">
            <colgroup>
                <col width="5%"/>
                <col width="55%"/>
                <col width="10%"/>
                <col width="10%"/>
                <col width="10%"/>
                <col width="10%"/>
            </colgroup>
            <tr>
                <th>#</th>
                <th>Наименование</th>
                <th>Вес</th>
                <th>Цена</th>
                <th>Количество</th>
                <th>Итого</th>
                <td> </td>
            </tr>

            <?php
            $i = 0;
            ?>
            <?php foreach ($cur['products'] as $row): ?>
                <tr>
                    <td><?php echo ++$i ?></td>
                    <td><?= $row['name'] ?></td>
                    <td><?= $row['weight'] ?></td>
                    <td><?= $row['price'] ?></td>
                    <td><?= $row['count'] ?></td>
                    <td><?= $row['total'] ?></td>
                    <td><?php if($this->isAdmin()):?>
                        <div class="admin-remove"><?php echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', ['site/removeOrder', 'date'=>$date,'id'=>$row['order_id']], ['class'=>'btn btn-danger']);?></div>
                        <?php endif;?>
                    </td>
                </tr>

            <?php endforeach; ?>
            <tr>
                <td></td>
                <td></td>
                <td> </td>
                <td> </td>
                <td style="text-align: right">Сумма</td>
                <td><?php echo $cur['sum']; ?></td>
                <td> </td>

            </tr>
            <tr>
                <td></td>
                <td></td>
                <td> </td>
                <th colspan="2" style="text-align: right"> 
                    Сумма c доставкой</th>
                <th><?php echo $cur['total']; ?></th><td> </td>

            </tr>
        </table>
    <?php endforeach; ?>

    <h5>Доставка: <?php echo $orders['delivery']; ?> Руб.</h5>
    <h5>Сумма: <?php echo $orders['sum']; ?> Руб.</h5>
    <?php if($orders['sum'] < 1500):?>
    <h6>До бесплатной доставки не хватает <?php echo 1500-$orders['sum']; ?> Руб.</h6>
    <?php else:?>
    <h6>Бесплатная доставка</h6>
    <?php endif;?>
    <h4>Сумма с доставкой: <?php echo $orders['total']; ?> Руб.</h4>
    <?php if($this->isAdmin()):?>
    <form action="<?php echo Yii::app()->createUrl('site/download');?>" method="post">
        <input type="hidden" name="download" value="<?php echo $date; ?>" />
        <div class="admin-remove"><button type="submit" class="btn btn-primary">Скачать заказ <span class="glyphicon glyphicon-download"></span></button></div>
    </form>
    <?php endif; ?>
<?php endif; ?>

<p> </p>