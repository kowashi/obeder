<div class="container">

    <form method="POST" action="" accept-charset="UTF-8">
        <div class="row">
            <legend>Введите электропочту и пароль</legend>            
            <div class="col-sm-3">
                <label>Почта</label>
                <input type="text" id="username" class="form-control" value="<?= !empty($_POST['username']) ? $_POST['username'] : '' ?>" name="username" placeholder="obeder@acquiropay.com">
            </div>
            <div class="col-sm-2">
                <label>Пароль:</label>
                <input type="password" id="password" class="form-control" value="<?= !empty($_POST['password']) ? $_POST['password'] : '' ?>" name="password" placeholder="Пароль">                    
            </div>
        </div>        
        <?php if (!empty($error)): ?>
            <div class="row">
                <div class="col-sm-4">
                    <p class="text-danger">Такая почта или пароль не существуют.</p>
                </div>
            </div>
        <?php endif; ?>        
        <div class="row col-sm-2">
            <p><?php echo CHtml::link('Забыл пароль', ['site/recovery']); ?> <?php echo CHtml::link('Регистрация', ['site/register']); ?></p>
            <button type="submit" name="submit" class="btn btn-info">Войти <span class="glyphicon glyphicon-triangle-right"></span></button>            

        </div>
    </form>
</div>

