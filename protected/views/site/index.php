<?php $this->title = ' - Меню на '.Obeder::currentDate($date); ?>

<h3>Меню на <?php echo Obeder::currentDate($date)?> <small><?php echo date('d.m.y', $date)?></small></h3>
<div>


    <?php
    $this->widget('zii.widgets.CMenu', array(
        'items' => array(
            array('active' => $dayofweek == 1, 'label' => 'Понедельник', 'url' => array('site/index', 'dayofweek' => 1)),
            array('active' => $dayofweek == 2, 'label' => 'Вторник', 'url' => array('site/index', 'dayofweek' => 2)),
            array('active' => $dayofweek == 3, 'label' => 'Среда', 'url' => array('site/index', 'dayofweek' => 3)),
            array('active' => $dayofweek == 4, 'label' => 'Четверг', 'url' => array('site/index', 'dayofweek' => 4)),
            array('active' => $dayofweek == 5, 'label' => 'Пятница', 'url' => array('site/index', 'dayofweek' => 5)),
            array(
                'label' => 'Следующая неделя <span class="caret"></span>',
                'url' => '#',
                'encodeLabel' => false,
                'items' => array(
                    array('active' => $dayofweek == 6, 'label' => 'Понедельник', 'url' => array('site/index', 'dayofweek' => 6)),
                    array('active' => $dayofweek == 7, 'label' => 'Вторник', 'url' => array('site/index', 'dayofweek' => 7)),
                    array('active' => $dayofweek == 8, 'label' => 'Среда', 'url' => array('site/index', 'dayofweek' => 8)),
                    array('active' => $dayofweek == 9, 'label' => 'Четверг', 'url' => array('site/index', 'dayofweek' => 9)),
                    array('active' => $dayofweek == 10, 'label' => 'Пятница', 'url' => array('site/index', 'dayofweek' => 10)),
                ),
                'linkOptions' => [
                    'class' => "dropdown-toggle",
                    'data-toggle' => "dropdown",
                    'role' => "button",
                    'aria-expanded' => "false"],
                'submenuOptions' => ['class' => "dropdown-menu", 'role' => "menu"]
            ),
        ),
        'htmlOptions' => ['class' => 'nav nav-tabs'],
    ));
    ?>



</div>
<?php if (Yii::app()->user->isGuest): ?>
    <?php if ($menu == false): ?>
        <h3 class="bg-warning">Меню на <?php echo date('d.m.Y', $date); ?> еще не сформированно</h3>
    <?php else: ?>
        <?php foreach ($menu as $category): ?>
            <h3><?php echo $category['name'] ?></h3>
            <table class="table">
                <colgroup>
                    <col width="5%"/>
                    <col width="75%"/>
                    <col width="10%"/>
                    <col width="10%"/>                
                </colgroup>
                <tr>
                    <th style="border-top:none;">#</th>
                    <th style="border-top:none;">Наименование</th>
                    <th style="border-top:none;">Вес</th>
                    <th style="border-top:none;">Цена</th>
                </tr>
                <?php $i = 0; ?>
                <?php foreach ($category['products'] as $row): ?>

                    <tr>
                        <td><?php echo ++$i ?></td>
                        <td><?= $row['name'] ?></td>
                        <td><?= $row['weight'] ?></td>
                        <td><?= $row['price'] ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endforeach; ?>
    <?php endif; ?>
<?php else: ?>
    <?php echo $this->renderPartial('_user_menu', ['menu' => $menu, 'date' => $date, 'dayofweek'=>$dayofweek]); ?>
<?php endif; ?>

