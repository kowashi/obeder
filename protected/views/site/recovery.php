<div class="container">
    <?php if($sended):?>
    <h2>Ваш новый пароль выслан вам на почту</h2>
    <?php else: ?>
    <form method="POST" action="" accept-charset="UTF-8">
        <div class="row">
            <legend>Введите электропочту</legend>            
            <div class="col-sm-3">
                <label>Почта</label>
                <input type="text" id="username" class="form-control" value="<?= !empty($_POST['username']) ? $_POST['username'] : '' ?>" name="username" placeholder="obeder@acquiropay.com">
            </div>
        </div>        
        <?php if (!empty($error)): ?>
            <div class="row">
                <div class="col-sm-4">
                    <p class="text-danger">Такая почта не существует.</p>
                </div>
            </div>
        <?php endif; ?>        
        <div class="row col-sm-2">
            <p><?php echo CHtml::link('Регистрация', ['site/register']); ?></p>
            <button type="submit" name="submit" class="btn btn-info">Вспомнить</button>            

        </div>
    </form>
    <?php endif;?>
</div>