<?php if ($menu == false): ?>
    <h3 class="bg-warning">Меню на <?php echo date('d.m.Y', $date); ?> еще не сформированно</h3>
<?php else: ?>
    <?php foreach ($menu as $category): ?>
        <h3><?php echo $category['name'] ?></h3>
        <table class="table">
            <colgroup>
                <col width="5%"/>
                <col width="65%"/>
                <col width="10%"/>
                <col width="10%"/>                
                <col width="10%"/>
            </colgroup>
            <tr>
                <th style="border-top:none;">#</th>
                <th style="border-top:none;">Наименование</th>
                <th style="border-top:none;">Вес</th>
                <th style="border-top:none;">Цена</th>       
                <th style="border-top:none;">Заказ</th>

            </tr>
            <?php $i = 0; ?>
            <?php foreach ($category['products'] as $row): ?>
            
                <tr>
                    <td><?php echo ++$i ?></td>
                    <td><?= $row['name'] ?></td>
                    <td><?= $row['weight'] ?></td>
                    <td><?= $row['price'] ?></td>            

                    <td>
                        <?php if (empty($row['count'])): ?>                
                            <span class="btn btn-default"><i class="glyphicon glyphicon-minus"></i></span>
                            <b>0</b>
                        <?php else: ?>
                            <?php echo CHtml::link('<i class="glyphicon glyphicon-minus"></i>', ['site/unbuy', 'id' => $row['id'], 'date' => $date,'dayofweek'=>$dayofweek], ['class' => 'btn btn-danger']); ?>
                            <b><?= $row['count']; ?></b>
                        <?php endif; ?>


                        <?= CHtml::link('<i class="glyphicon glyphicon-plus"></i>', ['site/buy', 'id' => $row['id'], 'date' => $date, 'dayofweek'=>$dayofweek], ['class' => 'btn btn-success']); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php endforeach; ?>
<?php endif; ?>

<script>
    $(function(){
        $(document).on('click', '.table a.btn', function(){
            var t = $(this),
                minusBtn = t.parents('td').eq(0).children().eq(0),
                cntWrap = t.siblings('b'),
                newCnt = (t.hasClass('btn-success')) ? +cntWrap.text() + 1 : (cntWrap.text() == '0') ? '0' : +cntWrap.text() - 1,
                id, date, dayofweek, href, tmp;
            $.ajax(t.attr('href'));
            cntWrap.text(newCnt);
            if (newCnt > 0) {
                href = t.parents('td').eq(0).children(':last').attr('href');
                tmp = href.substr(href.indexOf('id=') + 3);
                id = tmp.substring(0, tmp.indexOf('&'));
                tmp = href.substr(href.indexOf('date=') + 5);
                date = tmp.substring(0, tmp.indexOf('&'));
                dayofweek = href.substr(href.indexOf('dayofweek=') + 10);
                minusBtn.replaceWith('<a class="btn btn-danger" href="/index.php?r=site/unbuy&id=' + id + '&date=' + date + '&dayofweek=' + dayofweek + '"><i class="glyphicon glyphicon-minus"></i></a>');
            } else {
                minusBtn.replaceWith('<span class="btn btn-default"><i class="glyphicon glyphicon-minus"></i></span>');
            }
            return false;
        });
    });
</script>