<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title><?= Yii::app()->name . $this->title ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <style>
            body{
                background: url("css/images/pattern.png") repeat scroll 0 0 #fff;
            }
            .main{
                background-color: #fff;
            }
        </style>

        <!-- Latest compiled and minified JavaScript -->
        <script src="js/jquery.js"></script>
        <script src="js/jquery.animate-colors-min.js"></script>

        <script src="js/bootstrap.min.js"></script> 
    </head>
    <body>
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">

                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items' => array(
                        array(
                            'encodeLabel' => false,
                            'label' => '<span class="glyphicon glyphicon-home"></span>',
                            'url' => array('site/index')),
                        array(
                            'encodeLabel' => false,
                            'label' => 'Список заказов <span class="glyphicon glyphicon-list-alt"></span>',
                            'url' => array('site/order'),
                            'visible' => !Yii::app()->user->isGuest),
                        array(
                            'encodeLabel' => false,
                            'label' => 'Войти <span class="glyphicon glyphicon-log-in"></span>',
                            'url' => array('site/login'),
                            'visible' => Yii::app()->user->isGuest),
                        array(
                            'encodeLabel' => false,
                            'label' => Yii::app()->user->getState('nickname') . ' <span class="glyphicon glyphicon-log-out"></span>',
                            'url' => array('site/logout'),
                            'visible' => !Yii::app()->user->isGuest),
                    ),
                    'htmlOptions' => ['class' => 'nav navbar-nav'],
                ));
                ?>




            </div>
        </div>
        <div class="container main">
            <?php echo $content; ?>
        </div>    
    </body>
</html>
